using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace JumpVertical
{
    [CustomEditor(typeof(PartController))]
    public class PartCustomInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            PartController partController = (PartController)target;

            if (GUILayout.Button("Auto Genarate Path"))
            {
                partController.AutoGenaratePath();
            }
            if (GUILayout.Button("Set Default"))
            {
                partController.SetDefault();
            }
        }
    }
}