using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JumpVertical
{
    [CreateAssetMenu(fileName = "LevelConfig", menuName = "ScriptableObject/LevelConfig")]
    public class LevelConfig : ScriptableObject
    {
        public int LevelID;
        public PartController Part;
    }
}
