﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

namespace JumpVertical
{
    public class PlayerInputController : MonoBehaviour
    {
        public float UpdateInput()
        {
            return Input.GetAxis("Horizontal");
        }
    }
}