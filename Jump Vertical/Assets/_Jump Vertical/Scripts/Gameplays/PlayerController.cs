using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deVoid.Utils;
using System;
using Cysharp.Threading.Tasks;

namespace JumpVertical
{
    public class PlayerController : MonoBehaviour
    {
        private void Awake()
        {
            Signals.Get<AS_FailPart>().AddListener(OnFailPart);
        }

        private void OnFailPart()
        {
            print("OnFailPart");
            Destroy(gameObject);
        }
    }
}