using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JumpVertical
{
    public class PlayerMovementController : MonoBehaviour
    {
        [SerializeField] private float _movementSpeed = 10f;
        private Rigidbody2D _rb;
        private float _movement = 0f;
        private PlayerInputController _playerInput;

        // Use this for initialization
        private void Awake()
        {
            _rb = GetComponent<Rigidbody2D>();
            _playerInput = GetComponent<PlayerInputController>();
        }

        // Update is called once per frame
        private void Update()
        {
            _movement = _playerInput.UpdateInput() * _movementSpeed;
        }

        private void FixedUpdate()
        {
            Vector2 velocity = _rb.velocity;
            velocity.x = _movement;
            _rb.velocity = velocity;
        }
    }
}