using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deVoid.Utils;
using System;

namespace JumpVertical
{
    public class CameraFlow : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        private bool _isFlowing = true;

        private void Awake()
        {
            Signals.Get<AS_FailPart>().AddListener(OnFailPart);
        }

        private void LateUpdate()
        {
            if (_isFlowing && _target.position.y > transform.position.y)
            {
                Vector3 newPos = new Vector3(transform.position.x, _target.position.y, transform.position.z);
                transform.position = newPos;
            }
        }

        private void OnFailPart()
        {
            _isFlowing = false;
        }
    }
}