using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JumpVertical
{
    public class PartController : MonoBehaviour
    {
        [SerializeField] private Transform _trsStart;
        [SerializeField] private Transform _trsEnd;
        [SerializeField] private GameObject _prfPlatformer;
        [SerializeField] private int _numberRow;
        [SerializeField] private Transform _trsRow;
        [SerializeField] private bool _isDone = false;
        [SerializeField] private TriggerFailPart _triggerFail;

        public Transform EndPoint { get => _trsEnd; }
        public Transform StartPoint { get => _trsStart; }
        public bool IsDone { get => _isDone; }

        public async void AutoGenaratePath()
        {
            print("DestroyAllChildren...");
            await TransformUtils.DestroyAllChildren(_trsRow);
            print("AutoGenaratePath...");
            float spacing = (_trsEnd.position.y - _trsStart.position.y) / _numberRow;
            Vector3 nowPosition = _trsStart.position;
            while (nowPosition.y < _trsEnd.position.y)
            {
                GameObject obj = Lean.Pool.LeanPool.Spawn(_prfPlatformer, nowPosition, Quaternion.identity, _trsRow);
                nowPosition.y += spacing;
                await UniTask.DelayFrame(1);
            }
            print("AutoGenaratePath... DONE");
        }

        public void DonePart()
        {
            _isDone = true;
        }

        public void SetDefault()
        {
            _triggerFail.ActiveTrigger();
        }
    }
}