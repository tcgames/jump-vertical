using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    [SerializeField] private float _jumpForce = 8;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Rigidbody2D rigi2D;
        if (collision.transform.CompareTag("Player") && collision.relativeVelocity.y <= 0 && collision.collider.TryGetComponent(out rigi2D))
        {
            Vector2 velocity = rigi2D.velocity;
            velocity.y = _jumpForce;
            rigi2D.velocity = velocity;
        }
    }
}