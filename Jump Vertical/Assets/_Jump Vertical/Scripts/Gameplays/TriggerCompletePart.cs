using deVoid.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JumpVertical
{
    public class TriggerCompletePart : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D obj)
        {
            if (obj.CompareTag("TriggerCompletePart"))
            {
                Signals.Get<AS_CompletePart>().Dispatch();
            }
        }
    }
}