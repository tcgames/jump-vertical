using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deVoid.Utils;
using System;

namespace JumpVertical
{
    public class TriggerFailPart : MonoBehaviour
    {
        [SerializeField] private bool _activeTrigger = false;

        private void Awake()
        {
            Signals.Get<AS_CompletePart>().AddListener(OnCompletePart);
        }

        private void OnTriggerEnter2D(Collider2D obj)
        {
            if (_activeTrigger && obj.CompareTag("Player"))
            {
                Signals.Get<AS_FailPart>().Dispatch();
            }
        }

        private void OnCompletePart()
        {
            ActiveTrigger();
        }

        public void ActiveTrigger()
        {
            _activeTrigger = true;
        }
    }
}