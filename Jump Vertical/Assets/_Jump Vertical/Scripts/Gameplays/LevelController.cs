using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
using JumpVertical.Utils;
using deVoid.Utils;

namespace JumpVertical
{
    public class LevelController : ManualSingletonMono<LevelController>
    {
        [SerializeField] private Transform _trsParts;
        [SerializeField] private PartController _oldPart;
        [SerializeField] private int _countPartToClear = 0;
        private float _spacing;
        LevelConfig _levelConfig;

        public int LevelID;

        public override void Awake()
        {
            base.Awake();
            Signals.Get<AS_CompletePart>().AddListener(OnCompletePart);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Signals.Get<AS_CompletePart>().AddListener(OnCompletePart);
        }

        private void Start()
        {
            StartLevel();
        }

        private void OnCompletePart()
        {
            if (_oldPart.IsDone) return;
            SetDoneOldPart();
            SpawnPart();
            AutoClear();
        }

        private void AutoClear()
        {
            if (++_countPartToClear >= 2)
            {
                _countPartToClear = 1;
                Destroy(_trsParts.GetChild(0).gameObject);
            }
        }

        private void SpawnPart(bool isDefault = false)
        {
            var part = _levelConfig.Part;
            _spacing = part.EndPoint.localPosition.y - part.StartPoint.localPosition.y;
            _oldPart = Instantiate(part, isDefault ? Vector3.zero : _oldPart.EndPoint.position + Vector3.up * _spacing / 2, Quaternion.identity, _trsParts);
        }

        private void SetDoneOldPart()
        {
            _oldPart.DonePart();
        }

        public void StartLevel()
        {
            print("StartLevel");
            SpawnPart(isDefault: true);
            _oldPart.SetDefault();
        }
    }
}