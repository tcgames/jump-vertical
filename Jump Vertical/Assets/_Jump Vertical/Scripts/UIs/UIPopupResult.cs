using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deVoid.UIFramework;
using JumpVertical;
using UnityEngine.UI;

public class UIPopupResult : APanelController
{
    [SerializeField] private Button _btnRestart;
    [SerializeField] private Button _btnMenu;
    protected override void Awake()
    {
        base.Awake();
        _btnRestart.onClick.AddListener(OnClickRestart);
        _btnMenu.onClick.AddListener(OnClickNenu);
    }

    private void OnClickNenu()
    {
        print("OnClickMenu");
    }

    private void OnClickRestart()
    {
        print("OnclickRestart");
    }

}
