using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deVoid.UIFramework;
using UnityEngine.UI;
using System;
using JumpVertical;

public class UIPopupPaused : APanelController
{
    [SerializeField] private Button _btnClose;
    [SerializeField] private Button _btnRestart;
    [SerializeField] private Button _btnMenu;

    protected override void Awake()
    {
        base.Awake();
        _btnClose.onClick.AddListener(OnClickClose);
        _btnRestart.onClick.AddListener(OnClickRestart);
        _btnMenu.onClick.AddListener(OnClickNenu);
    }

    private void OnClickNenu()
    {
        print("OnClickMenu");
    }

    private void OnClickRestart()
    {
        print("OnclickRestart");
    }

    private void OnClickClose()
    {
        UIFrameManager.Instance.uIFrame.HidePanel(ScreenIds.UIPopupPaused);
        print("OnClickClose");
    }
}