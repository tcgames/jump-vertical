using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deVoid.UIFramework;
using JumpVertical;
using deVoid.Utils;
using System;
using TMPro;

public class UILoading : APanelController
{
    [SerializeField] private TextMeshProUGUI _tmpProgress;

    protected override void AddListeners()
    {
        Signals.Get<AS_UpdateProgressLoading>().AddListener(OnAS_UpdateProgressLoading);
    }

    protected override void RemoveListeners()
    {
        Signals.Get<AS_UpdateProgressLoading>().RemoveListener(OnAS_UpdateProgressLoading);
    }

    private void OnAS_UpdateProgressLoading(int obj)
    {
        _tmpProgress.text = $"{obj}%";
    }
}