using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deVoid.UIFramework;
using JumpVertical;
using UnityEngine.UI;

public class UIPopupExit : APanelController
{
    [SerializeField] private Button _btnYes;
    [SerializeField] private Button _btnClose;
    protected override void Awake()
    {
        base.Awake();
        _btnYes.onClick.AddListener(OnClickYes);
        _btnClose.onClick.AddListener(OnClickClose);
    }

    private void OnClickYes()
    {
        print("OnclickYes");
    }

    private void OnClickClose()
    {
        UIFrameManager.Instance.uIFrame.HidePanel(ScreenIds.UIPopupPaused);
        print("OnClickClose");
    }
}
