﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using JumpVertical.Utils;
public class UserProfileManager : ManualSingletonMono<UserProfileManager>
{
    [SerializeField] private UserProfile m_userProfile;
    private string m_tempFilePath;
    private bool m_isNewer = false;

    public UserProfile UserProfile { get => m_userProfile; set => m_userProfile = value; }
    public bool IsNewer { get => m_isNewer; set => m_isNewer = value; }

    public void Init(Action OnComplete)
    {
        m_tempFilePath = Application.persistentDataPath + "/UserProfile.data";

#if UNITY_EDITOR
        Debug.Log("TempFilePath ==> " + m_tempFilePath);
#endif
        if (!File.Exists(m_tempFilePath))//make a file if not exists
        {
            m_isNewer = true;
            using (StreamWriter newTask = new StreamWriter(m_tempFilePath, false))
            {
                m_userProfile = new UserProfile();
                newTask.WriteLine(JsonUtility.ToJson(m_userProfile));
                newTask.Close();
            }
        }
        else// read and update file
        {
            StreamReader reader = new StreamReader(m_tempFilePath);
            JsonUtility.FromJsonOverwrite(reader.ReadLine(), m_userProfile);
            reader.Close();
        }

        OnComplete?.Invoke();
    }

    private void SaveData()
    {
        UserProfile.ResetScore();
        if (m_tempFilePath != null)
        {
            using (StreamWriter newTask = new StreamWriter(m_tempFilePath, false))
            {
                string str = JsonUtility.ToJson(m_userProfile);
                newTask.WriteLine(str);
                newTask.Close();
            }
        }
    }

    private void OnApplicationPause(bool pause)
    {
        SaveData();
    }

    private void OnApplicationQuit()
    {
        SaveData();
    }
}

[System.Serializable]
public class UserProfile
{
    public int Score;
    public int BestScore;
    public int Apples;
    public int IndexLevel;
    public int CurrentWeapon = 1;
    public List<int> Weapons = new List<int>();

    public UserProfile()
    {
        Apples = 0;
        IndexLevel = 0;
    }

    public void ResetScore()
    {
        Score = 0;
    }
}