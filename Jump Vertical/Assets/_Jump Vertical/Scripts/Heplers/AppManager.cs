using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Skywatch.AssetManagement;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;
using JumpVertical.Utils;

namespace JumpVertical
{
    public class AppManager : AutoSingletonMono<AppManager>
    {
        public Dictionary<string, GameObject> ObjectsGame = new Dictionary<string, GameObject>();
        public Dictionary<int, LevelConfig> LevelsConfig = new Dictionary<int, LevelConfig>();

        public async void ChangeScene(SceneName sceneName, Action onSceneLoaded = null)
        {
            await Addressables.LoadSceneAsync(sceneName.ToString());
        }

        public AsyncOperationHandle InitObjectsGame()
        {
            AsyncOperationHandle loader = AssetManager.LoadAssetsByLabelAsync("objectsgame");
            loader.Completed += op =>
            {
                List<object> results = op.Result as List<object>;
                foreach (object obj in results)
                {
                    GameObject item = obj as GameObject;
                    if (!ObjectsGame.ContainsKey(item.name))
                    {
                        ObjectsGame.Add(item.name, item);
                    }
                }
                print($"InitObjectsGame {ObjectsGame.Count}");
            };
            return loader;
        }

        public AsyncOperationHandle InitLevelsConfig()
        {
            AsyncOperationHandle loader = AssetManager.LoadAssetsByLabelAsync("LevelsConfig");
            loader.Completed += op =>
            {
                List<object> results = op.Result as List<object>;
                foreach (object obj in results)
                {
                    LevelConfig item = obj as LevelConfig;
                    int id = item.LevelID;
                    if (!LevelsConfig.ContainsKey(id))
                    {
                        LevelsConfig.Add(id, item);
                    }
                }
                print($"InitLevelsConfig {LevelsConfig.Count}");
            };
            return loader;
        }
    }
}
