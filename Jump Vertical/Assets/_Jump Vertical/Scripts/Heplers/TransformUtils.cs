using UnityEngine;
using Cysharp.Threading.Tasks;

public static class TransformUtils
{
    public static async UniTask DestroyAllChildren(Transform parent)
    {
        for (var i = 0; i < parent.childCount; i++)
        {
            Object.DestroyImmediate(parent.GetChild(i)
              .gameObject);
            await UniTask.DelayFrame(1);
        }
    }

    public static void HideAllChildren(Transform parent)
    {
        for (var i = 0; i < parent.childCount; i++)
            parent.GetChild(i)
                .gameObject.SetActive(false);
    }

    public static T GetFirstComponentInUpper<T>(Transform t) where T : MonoBehaviour
    {
        var parent = t.parent;
        while (parent != null)
        {
            var com = parent.GetComponent<T>();
            if (com != null)
            {
                return com;
            }

            parent = parent.parent;
        }

        return null;
    }
}