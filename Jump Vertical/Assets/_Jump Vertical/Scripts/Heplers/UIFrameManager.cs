using System.Collections;
using System.Collections.Generic;
using deVoid.UIFramework;
using UnityEngine;
using JumpVertical.Utils;

public class UIFrameManager : ManualSingletonMono<UIFrameManager>
{
    [SerializeField] private List<UISettings> uISettings;
    public UIFrame uIFrame { get; private set; }

    // Start is called before the first frame update
    public override void Awake()
    {
        base.Awake();
        foreach (UISettings uiSetting in uISettings)
            uIFrame = uiSetting.CreateUIInstance(true, uIFrame);
    }
}
