﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deVoid.Utils;

namespace JumpVertical
{
    public class AS_UpdateProgressLoading : ASignal<int>
    {
    }

    public class AS_CompletePart : ASignal
    {
    }

    public class AS_FailPart : ASignal
    {
    }
}