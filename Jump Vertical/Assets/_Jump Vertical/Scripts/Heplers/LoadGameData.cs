using Cysharp.Threading.Tasks;
using deVoid.Utils;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace JumpVertical
{
    public class LoadGameData : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start()
        {
            UIFrameManager.Instance.uIFrame.ShowPanel(ScreenIds.UILoading);
            LoadData();
        }

        private async void LoadData()
        {
            List<UniTask> tasks = new List<UniTask>() {
                AppManager.Instance.InitObjectsGame().ToUniTask()
            };

            await UniTask.WhenAll(tasks);

            DOVirtual.Float(50, 100, 3, (x) =>
            {
                Signals.Get<AS_UpdateProgressLoading>().Dispatch((int)x);
            }).OnComplete(() =>
            {
                AppManager.Instance.ChangeScene(SceneName.Main);
            });
        }
    }
}